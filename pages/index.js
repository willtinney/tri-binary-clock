import Head from 'next/head'
import Canvas from '../components/Canvas'
import Timer from '../components/Timer'

const Index = () => (
  <div className='app'>
    <Head>
      <title>Sennep Binary Clock</title>
      <link rel='shortcut icon' type='image/x-icon' href='../static/assets/favicon.ico' />
      <link href='https://fonts.googleapis.com/css?family=Josefin+Sans|Lato|Oxygen' rel='stylesheet' />
    </Head>
    <h3>Triangular Binary Clock</h3>
    <div className='clock'>
      <Canvas />
      <Timer />
    </div>
  </div>
)

// If hours/minutes/seconds = 12:00:00 or 00:00:00 then refresh page
// Get seconds to not delay on start of minute
// link timer font to size of triangle
// where does .gitignore go
// how to set favicon on nextjs

export default Index
