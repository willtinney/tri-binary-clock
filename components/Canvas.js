class Canvas extends React.Component {
  colors = {
    // Core colours array
    background: "#FEECE3",
    text: "#061BC3",
    off: "#061BC3",
    on: "#FD5962",
    outline: "#EBC2BD",
    seconds: "#80B5DD"
  }

  textSize = {
    timer: 26
  }

  state = {
    // Set empty rows state
    rows: [0, 0, 0, 0, 0],
    offset: 1000,
    currentSeconds: 0,
    targetSeconds: 0
  }

  constructor (props) {
    super(props)
    let {second} = this.getTime()
    this.state.currentSeconds = second
    this.state.targetSeconds = second
  }

  setSize() {
    return 45
  }

  createTriangle(ctx) {
    // Fill canvas background
    ctx.fillStyle = this.colors['background']
    ctx.fillRect(0, 0, 300, 300)

    // Coordiantes of spacing between dots sets size
    let spacing = this.setSize()

    // Set coordiantes of each point
    let points = this.initializePoints(spacing)

    // Draw points
    points.forEach((row, i) => {
      // If row has more than one active point => connect dots
      if (this.state.rows[i] > 1) {
        this.connectDots(row[0]['x'], row[0]['y'],
          ctx, spacing, this.state.rows[i])
        // Then draw inactive dots
        row.forEach(point => {
          if (!point['active']){
            this.createPoint(point['x'], point['y'], ctx, spacing, point['active'])
          }
        })
      } else {
        // Else just draw inactive dots
        row.forEach(point => {
          this.createPoint(point['x'], point['y'], ctx, spacing, point['active'])
        })
      }
    })
  }

  initializePoints(spacing) {
    // Initialize layout and coordiantes of clock points
    let pointArray = []

    // Set size of border triangle
    let svgSize = spacing * (800 / 45)
    this.setState({
      svgSize: svgSize
    })

    let timerSize = spacing / 1.8
    this.textSize = {
      timer: timerSize
    }

    // Coordiantes of bottom-left point
    let x = (spacing * 1.375)
    let y = (spacing * 5.59)

    let i = 0
    let numberOfDots
    // Loop through rows
    for (numberOfDots = 5; numberOfDots > 0; numberOfDots--) {
      let point
      let pointRow = []
      // Loop through points
      for (point = 0; point < numberOfDots; point++) {
        // Add individual points to a row array
        pointRow.push({
          x: x,
          y: y,
          active: point + 1 <= this.state.rows[i] ? true : false
        })
        x += spacing
      }
      // Push array of points to main array
      pointArray.push(pointRow)

      // Adjust coordinates for first point of next row
      y -= spacing
      x -= spacing * numberOfDots - spacing / 2
      i++
    }

    return pointArray
  }

  createPoint(x, y, ctx, spacing, f) {
    // Creates individual point
    ctx.beginPath()
    ctx.arc(x, y, spacing / 7, 0, Math.PI * 2, true)

    if (f) {
      ctx.fillStyle = this.colors['on']
      ctx.fill()
    } else {
      ctx.fillStyle = this.colors['off']
      ctx.fill()
    }
  }

  connectDots(x, y, ctx, spacing, length) {
    // Draws line when multiple dots in a row
    ctx.beginPath()
    ctx.moveTo(x, y)
    ctx.lineTo(x + spacing * (length - 1), y)
    ctx.strokeStyle = this.colors['on']
    ctx.lineWidth = spacing / 4
    ctx.lineCap = 'round'
    ctx.stroke()
  }

  getTime() {
    // Returns digital clock
    const time = new Date
    const second = time.getSeconds()
    const minute = time.getMinutes()
    const hour = time.getHours()

    return {
      hour,
      minute,
      second
    }
  }

  checkMidday() {
    let {hour} = this.getTime()
    if (hour >= 12) {
      // Set PM colours
      this.colors = {
        background: "#020866",
        text: "#F4FAFD",
        on: "#69CEC4",
        off: "#071CD0",
        outline: "#4960AD",
        seconds: "#80B5DD"
      }
    } else {
      // Set AM colours
      this.colors = {
        background: "#FEECE3",
        text: "#061BC3",
        off: "#061BC3",
        on: "#FD5962",
        outline: "#EBC2BD",
        seconds: "#80B5DD"
      }
    }
  }

  clearCanvas() {
    const ctx = this.refs.canvas.getContext('2d')
    ctx.beginPath()
    ctx.moveTo(0, 0)
    ctx.clearRect(0, 0, 300, 300)
  }

  updateClock() {
    // Sets loop for running clock
    let {
      currentSeconds,
      targetSeconds
    } = this.state

    const ctx = this.refs.canvas.getContext('2d')
    this.clearCanvas()
    let {second, minute, hour} = this.getTime()
    let current = second !== 0
      ? currentSeconds + (targetSeconds - currentSeconds) * 0.1
      : 0

    // Allows for 24hr clock
    if (hour >= 12) {
      hour -= 12
    }

    // Check if AM or PM
    this.checkMidday()

    // update rows
    let rows = []

    // Sets correct amount of dots for each row
    rows[4] = Math.floor(hour / 6)
    hour -= 6 * rows[4]

    rows[3] = Math.floor(hour / 2)
    hour -= 2 * rows[3]

    rows[2] = hour * 2 + Math.floor(minute / 30)
    minute >= 30 ? minute -= 30 : minute = minute

    rows[1] = Math.floor(minute / 6)
    minute -= 6 * rows[1]

    rows[0] = minute

    this.createTriangle(ctx)
    this.setState({
      currentSeconds: current,
      targetSeconds: second,
      offset: currentSeconds / 60 * -784,
      rows
    })
    setTimeout(this.updateClock.bind(this), 100)
  }

  componentDidMount() {
    this.updateClock()
    this.initializePoints()
  }

  componentWillMount() {
    this.checkMidday()
  }

  render() {
    return (
      <div style={{ /*marginLeft: 40,*/ position: 'relative' }}>
        <canvas ref="canvas" width={this.state.svgSize / 2.5} height={this.state.svgSize / 2.5} />
        <svg
          width={this.state.svgSize + 'px'}
          height={this.state.svgSize + 'px'}
          viewBox="0 0 800 790"
          preserveAspectRatio="xMidYMid meet"
          style={{position: 'absolute', left: '0px', top:'0px', zIndex: 2}} >
          <rect id="svgEditorBackground" x="0" y="0" width="800" height="790"
            style={{fill: 'none', stroke: 'none'}}/>
          <path
            d="M152,40,
            152,40q4.96875,-0.05,10,10,
            L276,253,
            276,253q7.25,12.7,-17.016,12.1,
            L45,265,
            45.08,265.001q-23.6,0.06,-17.03,-12.06,
            L142,50,
            141.98,50.02q5.36,-10.2,10.2,-10.125"
            stroke={this.colors['seconds']}
            id="e340_arc2"
            style={{strokeWidth: '2px', fill: 'none', strokeDasharray: 784}}
          />
        </svg>
        <svg
          width={this.state.svgSize + 'px'}
          height={this.state.svgSize + 'px'}
          viewBox="0 0 800 790"
          preserveAspectRatio="xMidYMid meet"
          style={{position: 'absolute', left: '0px', top:'0px', zIndex: 2}} >
          <rect id="svgEditorBackground" x="0" y="0" width="800" height="790"
            style={{fill: 'none', stroke: 'none'}}/>
          <path
            d="M152,40,
            152,40q4.96875,-0.05,10,10,
            L276,253,
            276,253q7.25,12.7,-17.016,12.1,
            L45,265,
            45.08,265.001q-23.6,0.06,-17.03,-12.06,
            L142,50,
            141.98,50.02q5.36,-10.2,10.2,-10.125"
            stroke={this.colors['outline']}
            id="e340_arc2"
            strokeDashoffset={this.state.offset}
            style={{strokeWidth: '2px', fill: 'none', strokeDasharray: 784}}
          />
        </svg>
        <style global jsx>{`
          body {
            background: ${this.colors.background};
            font-family: 'Josefin Sans', sans-serif;
          }
          h2,h3 {
            color: ${this.colors.text};
          }
          #timer {
            margin: 0 114px;
            font-size: ${this.textSize.timer + 'px'};
          }
          .clock {
            margin: 50px 200px;
          }
        `}</style>
      </div>
    )
  }
}

export default Canvas
