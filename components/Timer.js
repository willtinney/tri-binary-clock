class Timer extends React.Component {
  state = {
    time: 'hh:mm:ss'
  }

  standardizeDigits(t) {
    // Adds 0 to single digits
    t = t.toString()
    return (t.length == 1) ? "0" + t : t
  }

  getTime() {
    // Returns digital clock
    const time = new Date
    const second = this.standardizeDigits(time.getSeconds())
    const minute = this.standardizeDigits(time.getMinutes())
    const hour = this.standardizeDigits(time.getHours())

    let timeString = hour + ":" + minute + ":" + second

    this.setState({
      time: timeString,
    })

    setTimeout(this.getTime.bind(this), 1000)
  }

  componentDidMount() {
    this.getTime()
  }

  render() {
    return (
      <h2 id='timer'>{this.state.time}</h2>
    )
  }
}

export default Timer
